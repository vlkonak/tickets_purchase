from django import forms
from models import RouteNode, Trip, Station, Ticket


class PurchaseTicketForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ('customer_first_name', 'customer_last_name', 'email', 'bed', 'tea')


class RouteNodeForm(forms.ModelForm):
    def __init__(self, **kwargs):
        super(RouteNodeForm, self).__init__(**kwargs)
        self.fields['arrival'].required = False
        self.fields['departure'].required = False

    class Meta:
        model = RouteNode
        fields = [
            'route',
            'station',
            'arrival',
            'departure',
            'duration',
        ]
        widgets = {
            'arrival': forms.TextInput(attrs={'class': 'timepicker'}),
            'departure': forms.TextInput(attrs={'class': 'timepicker'}),
            'duration': forms.TextInput(attrs={'class': 'duration-timepicker'}),
        }

    # TODO override save method to validate duration
        # TODO check not null arrival and departure
        # TODO don't allow same stations


# TODO maybe combine next together with separating only admin field
# TODO or maybe inherit on from another
class TripSearchForm(forms.Form):
    # TODO in future this fields must be text inputs, pulling stations dynamically
    from_where = forms.ChoiceField(choices=((x.id, x.name) for x in Station.objects.all()))
    to_where = forms.ChoiceField(choices=((x.id, x.name) for x in Station.objects.all()))
    date = forms.DateField(label='Date')


# for admin pages
class FilterTripForm(forms.Form):
    include_completed = forms.BooleanField(label='Include completed', initial=False, required=False)
    show_before = forms.DateField(required=False)
    show_after = forms.DateField(required=False)
