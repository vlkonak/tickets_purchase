# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect

from django.http import HttpResponse
from models import *

from django.views import View
from django.views.generic import ListView, DeleteView, UpdateView, CreateView, FormView, TemplateView

from forms import *

from django.urls import reverse, reverse_lazy

from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator

import datetime, json

# fucking spike for non ascii chars in dbs
import sys
reload(sys)
sys.setdefaultencoding('utf-8')


def index(request):
    return redirect('search')


class SearchTripView(ListView):
    model = Trip
    template_name = 'search_trains.html'

    def get_queryset(self):
        params = self.request.GET

        if not params:
            return super(SearchTripView, self).get_queryset()
        target_date = datetime.datetime.strptime(params['date'], "%Y-%m-%d").date()
        start_id = int(params['from_where'])
        end_id = int(params['to_where'])

        trips = Trip.fetch_by_date_and_stations(target_date, start_id, end_id)
        return trips

    def get_context_data(self, **kwargs):
        context = super(SearchTripView, self).get_context_data(**kwargs)
        context['form'] = TripSearchForm(self.request.GET)
        return context


class SelectSeatView(View):
    def get(self, request, trip):
        trip_id = self.kwargs.get('trip', None)
        trip_instance = Trip.objects.get(pk=trip_id)
        params = request.GET
        start = params.get('from')
        dest = params.get('to')

        trip_routenodes = RouteNode.objects.filter(route__trip=trip_instance)
        start_rn = trip_routenodes.get(station__id=start)
        dest_rn = trip_routenodes.get(station__id=dest)
        # fetch seats, which are occupied on the desired section of trip
        booked_tickets = Ticket.objects.filter(
            trip=trip_instance,
            canceled=False,
            boarding_station__duration__lt=dest_rn.duration,
            destination_station__duration__gt=start_rn.duration
        )
        available_seats = trip_instance.train.get_available_seats(
            booked_tickets=booked_tickets,
            single_dimension_seats_list=False
        )
        context = {
            'available_seats': available_seats,
            'trip': trip_instance,
            'station_from': start_rn.station,
            'station_to': dest_rn.station,
        }
        return render(request, 'select_seat.html', context)

    def post(self, request, trip):
        data = request.POST.dict()
        request.session['ticket_data'] = data
        return redirect('confirm_ticket')


class ConfirmTicketView(FormView):
    form_class = PurchaseTicketForm
    template_name = 'ticket_form.html'

    def get_context_data(self, **kwargs):
        context = super(ConfirmTicketView, self).get_context_data()
        data = self.request.session.get('ticket_data', {})
        trip = Trip.objects.get(pk=int(data.get('trip')))

        cost = trip.route.calc_voyage_cost(
            boarding_station=int(data['boarding_station']),
            destination_station=int(data['destination_station']),
            car_type=data['car_type']
        )
        board_st = Station.objects.get(pk=int(data['boarding_station']))
        dest_st = Station.objects.get(pk=int(data['destination_station']))

        context['ticket'] = {
            'trip': trip,
            'car': data['car'],
            'seat': data['seat'],
            'cost': cost,
            'boarding_station': board_st,
            'destination_station': dest_st,
            'date_departure': datetime.datetime.combine(trip.date_departure, trip.time_departure),
            'date_arrival': datetime.datetime.combine(trip.date_arrival, trip.time_arrival)
        }
        return context

    # if form valid - actually save model from form based on POST and SESSION data
    def form_valid(self, form):
        session_data = self.request.session.get('ticket_data')
        trip = Trip.objects.get(pk=session_data['trip'])
        rnodes = trip.route.routenode_set
        form.instance.trip = trip
        form.instance.boarding_station = rnodes.get(station_id=int(session_data['boarding_station']))
        form.instance.destination_station = rnodes.get(station_id=int(session_data['destination_station']))
        form.instance.car = session_data['car']
        form.instance.car_type = session_data['car_type']
        form.instance.seat = session_data['seat']
        form.instance.cost = trip.route.calc_voyage_cost(
            session_data['boarding_station'],
            session_data['destination_station'],
            session_data['car_type']
        )
        form.instance.save()
        try:
            del self.request.session['ticket_data']
        except KeyError:
            pass
        return super(ConfirmTicketView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy('thanks')


@method_decorator(user_passes_test(lambda u: u.is_superuser, '/admin/login'), name='dispatch')
class AdminView(View):
    pass


class EditRoute(AdminView):
    def get(self, request, route_id):
        route = Route.objects.get(pk=route_id)
        nodes = RouteNode.objects.filter(route=route_id).order_by('duration')
        stations = Station.objects.all()

        form = RouteNodeForm(initial={'route': route_id}, instance=route)

        data = {
            'route': route,
            'nodes': nodes,
            'stations': stations,
            'form': form,
        }
        return render(request, 'admin_edit_route.html', data)

    def post(self, request, route_id):
        form = RouteNodeForm(data=request.POST)
        if form.is_valid():
            form.save()
            return self.get(request, route_id)
        else:
            return HttpResponse(str(dict(form.errors)))


class DeleteRoute(DeleteView):
    model = Route

    def get_success_url(self):
        return reverse('manage_routes')


class UpdateNode(UpdateView):
    model = RouteNode
    fields = ['station', 'arrival', 'departure', 'duration']
    template_name_suffix = '_update_form'

    def get_success_url(self):
        return reverse('edit_route', args=[str(self.kwargs.get('route'))])


class DeleteNode(DeleteView):
    model = RouteNode

    def get_success_url(self):
        return reverse('edit_route', args=[str(self.kwargs.get('route'))])


class ManageRoutes(AdminView):
    def get(self, request):
        routes = Route.objects.all()
        context = {
            'routes': routes
        }
        return render(request, 'admin_manage_routes.html', context)

    def post(self, request):
        route = Route(description=request.POST['description'])
        route.save()
        return self.get(request)


class ManageTrips(AdminView, ListView):
    model = Trip
    template_name = 'admin_manage_trips.html'
    fields = ['route', 'date_departure', 'completed']

    # fetch list
    def get_queryset(self, **kwargs):
        params = self.request.GET
        # filter results using filter form
        results = self.model.objects.all()
        if params.get('include_completed') != 'on':
            results = results.exclude(completed=True)
        if params.get('show_after', ''):
            results = results.filter(date_departure__gt=params.get('show_after', ''))
        if params.get('show_before', ''):
            results = results.filter(date_departure__lt=params.get('show_before', ''))
        return results

    # squeeze filter and create forms
    def get_context_data(self, **kwargs):
        context = super(ManageTrips, self).get_context_data(**kwargs)
        context['filter_form'] = FilterTripForm(initial=self.request.GET)
        return context


class AddTripView(AdminView, CreateView):
    model = Trip
    fields = ('route', 'date_departure', 'date_arrival', 'train')

    def get_form(self, form_class=None):
        form = super(AddTripView, self).get_form()
        form.fields['route'].disabled = True
        return form

    def get_initial(self, **kwargs):
        initial = super(AddTripView, self).get_initial()
        initial['route'] = self.kwargs.get('route')
        initial['train'] = Train.objects.first()
        return initial

    def get_success_url(self):
        return reverse_lazy('manage_trips')


class ChangeTripView(AdminView, UpdateView):
    model = Trip
    fields = ('completed',)

    def get_response(self, request, *args, **kwargs):
        return "{'success':'true'}"

    def get_success_url(self):
        return reverse_lazy('manage_trips')


class ThanksView(TemplateView):
    template_name = 'thanks.html'


class TripReportView(AdminView, View):
    def get(self, request, trip_id):
        trip = Trip.objects.get(pk=trip_id)
        tickets = Ticket.objects.filter(trip=trip).order_by('car', 'seat', 'boarding_station__duration')
        context = {
            "trip": trip,
            "tickets": tickets
        }
        return render(request, 'tickets/trip_report.html', context)
