from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search', views.SearchTripView.as_view(), name='search'),
    url(r'^trip/(?P<trip>\d+)/select_seat', views.SelectSeatView.as_view(), name='select_seat'),
    url(r'^confirm_ticket$', views.ConfirmTicketView.as_view(), name='confirm_ticket'),

    url(r'^edit_route/(\d+)', views.EditRoute.as_view(), name='edit_route'),
    url(r'^manage_routes', views.ManageRoutes.as_view(), name='manage_routes'),
    url(r'^delete_route/(?P<pk>\d+)', views.DeleteRoute.as_view(), name='delete_route'),

    url(r'^manage_trips', views.ManageTrips.as_view(), name='manage_trips'),
    url(r'^add_trip/route/(?P<route>\d+){0,1}$', views.AddTripView.as_view(), name='add_trip'),
    url(r'^update_trip/(?P<pk>\d+)$', views.ChangeTripView.as_view(), name='update_trip'),

    url(r'^admin/report/trip/(?P<trip_id>\d+)', views.TripReportView.as_view(), name='trip_report'),


    url(r'^route/(?P<route>\d+)/delete_node/(?P<pk>\d+)/$',
        views.DeleteNode.as_view(), name='delete_node'),
    url(r'^route/(?P<route>\d+)/update_node/(?P<pk>[\d]+)/$',
        views.UpdateNode.as_view(), name='update_node'),
    url(r'^thanks', views.ThanksView.as_view(), name='thanks')
]
