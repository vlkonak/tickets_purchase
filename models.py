# -*- coding: utf-8 -*-
# from __future__ import unicode_literals
#
from django.db import models
from django.db.models import F, ExpressionWrapper
import django.contrib.postgres.fields as psql_fields

from django.contrib.auth.models import AbstractUser

from django.core.exceptions import ValidationError

import datetime


class City(models.Model):
    name = models.TextField()
    lon = models.FloatField()
    lat = models.FloatField()


class Station(models.Model):
    name = models.TextField()
    description = models.TextField()
    city = models.ForeignKey(City)

    def __str__(self):
        return self.name


class Route(models.Model):
    description = models.CharField(max_length=255, default="")
    active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.id) + ' ' + self.description

    def calc_voyage_cost(self, boarding_station, destination_station, car_type):
        car_type_coefficient = 20
        if car_type == 'c':
            car_type_coefficient = 30
        elif car_type == 'p':
            car_type_coefficient = 20

        start_rn = self.routenode_set.get(station_id=boarding_station)
        dest_rn = self.routenode_set.get(station_id=destination_station)
        count_result = self.routenode_set.filter(
            duration__gt=start_rn.duration,
            duration__lte=dest_rn.duration,
        ).aggregate(
            count=models.Count('id')
        )
        stations_count = count_result['count']
        return car_type_coefficient*stations_count


class RouteNode(models.Model):
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    station = models.ForeignKey(Station, on_delete=models.CASCADE)
    arrival = models.TimeField(null=True)
    departure = models.TimeField(null=True)
    duration = models.DurationField()  #time from first station till this node

    class Meta:
        ordering = ['-duration']

    def __str__(self):
        return str(self.station.name)


class Train(models.Model):
    SEATS_CAPACITY_PER_TYPE = {'p': 54, 'c': 36}
    cars = psql_fields.JSONField(default=dict)

    def get_available_seats(self, booked_tickets=models.QuerySet, single_dimension_seats_list=True):
        available_seats = {}
        for car_num, car_type in self.cars.items():
            total_seats_in_car = self.SEATS_CAPACITY_PER_TYPE[car_type]
            seats = [{
                'num': seat_no,
                'available': False if booked_tickets.filter(car=car_num, seat=seat_no).exists() else True
            } for seat_no in range(1, total_seats_in_car + 1)]
            if not single_dimension_seats_list:
                if car_type == 'p':
                    seats = [seats[1:36:2], seats[:36:2], seats[-18:]]
                elif car_type == 'c':
                    seats = [seats[1:36:2], seats[:36:2]]
            available_seats[int(car_num)] = {
                'type': car_type,
                'seats': seats
            }
        return available_seats


class Trip(models.Model):
    route = models.ForeignKey(Route, on_delete=models.CASCADE)
    date_departure = models.DateField(default=None)
    time_departure = models.TimeField(default=None)
    date_arrival = models.DateField(default=None)
    time_arrival = models.TimeField(default=None)
    completed = models.BooleanField(default=False)
    train = models.ForeignKey(Train)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.time_departure = self.route.routenode_set.last().departure
        self.time_arrival = self.route.routenode_set.first().arrival
        return super(Trip, self).save()

    def __str__(self):
        return 'tr:{} r:{} date:{}'.format(self.route, self.route.description,str(self.date_departure))

    @staticmethod
    def fetch_by_date_and_stations(target_date, board_station, dest_station):
        trips = Trip.objects.filter(
            route__routenode__station=board_station
        ).annotate(
            board_station_duration=F('route__routenode__duration'),
            board_station_departure=F('route__routenode__departure'),
            board_station_arrival=F('route__routenode__arrival')
        ).filter(
            route__routenode__station=dest_station,
            route__routenode__duration__gt=F('board_station_duration')
        ).annotate(
            dest_station_duration=F('route__routenode__duration'),
            dest_station_arrival=F('route__routenode__arrival'),
        ).filter(
            date_departure__gte=ExpressionWrapper(
                target_date - datetime.timedelta(days=1)
                + F('time_departure')
                - F('board_station_duration')
                , output_field=models.DateField()
            ),
            date_departure__lte=ExpressionWrapper(
                target_date
                + F('time_departure')
                - F('board_station_duration')
                , output_field=models.DateField()
            )
        ).annotate(
            d_time=models.ExpressionWrapper(
                target_date + F('board_station_departure'),
                output_field=models.DateTimeField()
            ),
            a_time=models.ExpressionWrapper(
                F('date_departure') + F('time_departure') + F('dest_station_duration'),
                output_field=models.DateTimeField()
            ),
        )
        return trips


class Ticket(models.Model):
    trip = models.ForeignKey(Trip, on_delete=models.CASCADE)
    car = models.IntegerField()
    seat = models.IntegerField()
    car_type = models.CharField(max_length=1)
    date_purchase = models.DateTimeField(auto_now=True)
    boarding_station = models.ForeignKey(RouteNode, related_name='board')
    destination_station = models.ForeignKey(RouteNode, related_name='destination')
    date_departure = models.DateTimeField()
    date_arrival = models.DateTimeField()
    customer_first_name = models.CharField(max_length=40)
    customer_last_name = models.CharField(max_length=40)
    email = models.EmailField()

    bed = models.BooleanField(default=False)
    tea = models.BooleanField(default=False)
    cost = models.FloatField()
    canceled = models.BooleanField(default=False)

    # inspect carefully if it valid method and rewrite it prettier
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        combine = datetime.datetime.combine
        time_to_dur = lambda x: combine(datetime.date.min, x) - datetime.datetime.min
        self.date_departure = combine(self.trip.date_departure, self.boarding_station.departure) \
                              + datetime.timedelta(days=(time_to_dur(self.trip.time_departure) + self.boarding_station.duration).days)
        self.date_arrival = combine(self.trip.date_departure, self.destination_station.arrival) \
                            + datetime.timedelta(days=(time_to_dur(self.trip.time_departure) + self.destination_station.duration).days)
        return super(Ticket, self).save()
